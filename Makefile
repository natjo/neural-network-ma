LOG=log
FILE=main.py

run:
	venv/bin/python $(FILE)

test:
	venv/bin/python tests/test.py

clean:
	ls -d log/current/** | grep -P '[A-Za-z_]+.[0-9]+_[0-9]+' | xargs -d"\n" rm -r
	rm -rf log/test_configs
	# rm -rf log/

launch_tensorboard:
	venv/bin/tensorboard --reload_interval 1 --logdir $(LOG)
