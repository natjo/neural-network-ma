from utils.network import Network
import utils.my_excepthook
import config

n = Network(networkName=config.LOG_NAME, fileWithTime=config.FILE_WITH_TIME)
n.train()
