import tensorflow as tf

tf.enable_eager_execution()


def main():
    # y = [0.9, 0.1, 0.8, 0.2]
    # y_ = [1, 0, 0, 1]
    Y = tf.constant([[0.1, 0.2, 0.3], [0.4, 0.5, 0.6], [0.7, 0.8, 0.9], ])
    Y_ = tf.constant([[1.0, 1.0, 0.0], [0.0, 0.0, 0.0], [1.0, 0.0, 0.0], ])
    # Y = tf.constant([[0.1, 0.2, 0.3], [0.4, 0.5, 0.6], [0.7, 0.8, 0.9], [0.1, 0.2, 0.3], [0.4, 0.5, 0.6], [0.7, 0.8, 0.9], ])
    # Y_ = tf.constant([[1.0, 1.0, 0.0], [0.0, 0.0, 0.0], [1.0, 0.0, 0.0], [1.0, 1.0, 0.0], [0.0, 0.0, 0.0], [1.0, 0.0, 0.0], ])
    define_ranking_loss(Y, Y_)

def define_ranking_loss(prediction, labels):
    stacked = tf.stack([prediction, labels], axis =1)
    print('stacked prediction/labels:\n {}'.format(stacked))
    elementwise_error = tf.map_fn(map_ranking_loss, stacked)
    print('elementwise error: {}'.format(elementwise_error))
    total_ranking_error = tf.reduce_mean(elementwise_error)
    print('total ranking error: {}'. format(total_ranking_error))
    return total_ranking_error

def map_ranking_loss(x):
    y = x[0]
    y_ = x[1]
    out = calc_ranking_loss(y, y_)
    print('----------------')
    return out

def calc_ranking_loss(y, y_):
    # pos = tf.where(tf.not_equal(y_, 0.0), y, tf.zeros_like(y))
    # neg = tf.where(tf.equal(y_, 0.0), y, tf.zeros_like(y))
    pos = tf.boolean_mask(y, tf.not_equal(y_, 0.0), axis=0)
    neg = tf.boolean_mask(y, tf.equal(y_, 0.0), axis=0)
    # print("Positive values:\n {}".format(pos))
    # print("Negative values:\n {}".format(neg))

    pos_ext = tf.expand_dims(pos, 1)
    neg_ext = tf.expand_dims(neg, 0)
    print("pos: {}".format(pos_ext))
    print("neg: {}".format(neg_ext))

    delta = tf.subtract(neg_ext, pos_ext)
    print('delta: {}'.format(delta))

    delta_adj = tf.multiply(delta, 5)
    print('delta_adj: {}'.format(delta_adj))

    exp = tf.exp(delta_adj)
    print('exp: {}'.format(exp))

    sum = tf.reduce_sum(exp)
    print('sum: {}'.format(sum))

    mean = tf.reduce_mean(exp)
    print('mean: {}'.format(mean))

    mean_no_nan = tf.where(tf.is_nan(mean), tf.zeros_like(mean), mean)
    print('mean_no_nan: {}'.format(mean_no_nan))

    return mean_no_nan

if __name__ == '__main__':
    main()
