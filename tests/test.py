import sys
import os
import glob
import random
import re
sys.path.append(os.path.abspath('.'))
import utils.my_excepthook
import tensorflow as tf
from datetime import datetime



tf.enable_eager_execution()

PERC_TRAIN = 0.8
DATASET_SIZE = 1000
BUFFER_SIZE = 100


def main():
    # parse_function('./data/mirflickr25/thumbnails/thumb_im1.jpg', './data/mirflickr25/multi_hot_encoding_tags/tags1.txt')
    IMAGE_PATHS_GLOB = './data/mirflickr25/thumbnails/*.jpg'
    LABEL_PATHS_GLOB = './data/mirflickr25/multi_hot_encoding_tags/*.txt'

    image_paths = sorted(glob.glob(IMAGE_PATHS_GLOB), key=sortKeyFunc)
    label_paths = sorted(glob.glob(LABEL_PATHS_GLOB), key=sortKeyFunc)
    image_paths = tf.constant(image_paths)
    label_paths = tf.constant(label_paths)

    dataset = tf.data.Dataset.from_tensor_slices((image_paths, label_paths))
    dataset = dataset.map(parse_function)

    train_dataset, test_dataset = split_train_test(dataset)


def split_train_test(dataset):
    train_size = int(PERC_TRAIN * DATASET_SIZE)

    dataset = dataset.shuffle(BUFFER_SIZE)
    train_dataset = dataset.take(train_size)
    test_dataset = dataset.skip(train_size)

    return train_dataset, test_dataset


def parse_function(filePath, labelPath):
    imageStr = tf.read_file(filePath)
    imageDecoded = tf.image.decode_jpeg(imageStr)

    labelStr = tf.read_file(labelPath)
    splitLabelStr = tf.string_split([labelStr], delimiter='\n')
    splitLabelStrDense = tf.sparse.to_dense(splitLabelStr, default_value='')[0]
    labelDecoded = tf.strings.to_number(splitLabelStrDense)
    return imageDecoded, labelDecoded


def sortKeyFunc(s):
    s = os.path.basename(s)[:-4]
    id_ = int(re.search(r'\d+', s).group())
    return id_


def check_random_label_feature_pairs(image_paths, label_paths):
    i = random.randint(0, len(image_paths))
    print(image_paths[i])
    print(label_paths[i])


if __name__ == '__main__':
    main()
