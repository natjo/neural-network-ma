from utils.network import Network
import tensorflow as tf
import utils.my_excepthook
import config
import datetime
import csv
import random as rand
rand.seed(datetime.datetime.now())



def main():
    random_search()


def random_search():
    config.EPOCHS=50
    cnt = 0
    numConfigs = 160
    time_per_epoch = 5 # seconds
    writer = init_csv_writer()
    for i in range(numConfigs):
        config.STEEPNESS = rand.uniform(0.0, 10.0)
        config.DROPOUT = rand.uniform(0.4,1.0)
        config.L2PENALTY = rand.uniform(1e-2, 1e-6)
        config.LEARNING_RATE = rand.uniform(1e-2, 1e-5)
        config.LR_DECAY = rand.uniform(0.9, 1.0)
        config.GRAD_DECAY = rand.uniform(0.8, 1.0)
        config.MOMENTUM = rand.uniform(0.8, 1.0)
        config.SIZE_ENC_FEATURES1 = rand.randint(200, 1000)
        config.SIZE_ENC_FEATURES2 = rand.randint(200, 1000)
        config.SIZE_ENC_LABELS1 = rand.randint(200, 1000)
        config.SIZE_ENC_OUTPUT = rand.randint(10, 60)
        config.SIZE_DEC1 = rand.randint(200, 1000)

        print_expected_finish_date((numConfigs - cnt)*config.EPOCHS*time_per_epoch)
        [max_micro_f1, max_macro_f1] = train_network()
        cnt += 1

        data = [i,config.STEEPNESS,config.DROPOUT,config.L2PENALTY,config.LEARNING_RATE,config.LR_DECAY,config.GRAD_DECAY,config.MOMENTUM,config.SIZE_ENC_FEATURES1,config.SIZE_ENC_FEATURES2,config.SIZE_ENC_LABELS1,config.SIZE_ENC_OUTPUT,config.SIZE_DEC1,max_micro_f1,max_macro_f1]
        writer.writerow(['{:3.4f}'.format(x) for x in data])



def init_csv_writer():
    csvfile = open('log/random_search/log.csv', 'w', newline='')
    writer = csv.writer(csvfile, delimiter=',', quotechar='|', quoting=csv.QUOTE_NONE)
    writer.writerow(['ID','STEEPNESS','DROPOUT','L2PENALTY','LEARNING_RATE','LR_DECAY','GRAD_DECAY','MOMENTUM','SIZE_ENC_FEATURES1','SIZE_ENC_FEATURES2','SIZE_ENC_LABELS1','SIZE_ENC_OUTPUT','SIZE_DEC1','MAX_MICRO_F1','MAX_MACRO_F1'])
    return writer


def grid_search():
    cnt = 0
    time_per_epoch = 7 # seconds
    numConfigs = 2 * 2 * 3 * 3

    for config.PERC_WITH_DROPOUT in [0.0, 0.5]:
        for config.L2PENALTY in [1e-3,5e-5]:
            for config.STEEPNESS in [1, 3, 5]:
                for config.SIZE_ENC_OUTPUT in [20, 30, 50]:
                    cnt += 1
                    print_config()
                    print_expected_finish_date((numConfigs - cnt)*config.EPOCHS*time_per_epoch)
                    train_network()


    print('Total number configs:', numConfigs)


def train_network():
    n = Network(networkName=config2String(), fileWithTime=False)
    n.train()
    max_micro_f1 = n.get_max_micro_f1()
    max_macro_f1 = n.get_max_macro_f1()
    tf.reset_default_graph()
    return [max_micro_f1, max_macro_f1]


def print_expected_finish_date(secondsLeft):
    t = datetime.datetime.now() + datetime.timedelta(seconds=secondsLeft)
    print('Expected end date: {}'.format(t.strftime('%d.%m - %H:%M')))


def print_config():
    print('config:', config2String())


def config2String():
    return "PwDropout" + str(config.PERC_WITH_DROPOUT) + "-steep" + str(config.STEEPNESS) + "-size_enc" + str(config.SIZE_ENC_OUTPUT) + "-l2pen" + str(config.L2PENALTY)


if __name__ == '__main__':
    main()
