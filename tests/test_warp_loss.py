import tensorflow as tf

tf.enable_eager_execution()

NUM_CLASSES = 4


def main():
    # y = [0.9, 0.1, 0.8, 0.2]
    # y_ = [1, 0, 0, 1]
    Y = tf.constant([[0.5, 0.2, 0.6, 0.8], [0.4, 0.5, 0.6, 0.2], [0.7, 0.8, 0.9, 0.1], ])
    Y_ = tf.constant([[1.0, 0.0, 0.0, 1.0], [0.0, 0.0, 0.0, 1.0], [1.0, 1.0, 0.0, 1.0], ])
    define_warp_loss(Y, Y_)

def define_warp_loss(prediction, labels):
    stacked = tf.stack([prediction, labels], axis =1)
    print('stacked prediction/labels:\n {}'.format(stacked))
    elementwise_error = tf.map_fn(map_warp_loss, stacked)
    print('elementwise error: {}'.format(elementwise_error))
    total_warp_error = tf.reduce_mean(elementwise_error)
    print('total warp error: {}'. format(total_warp_error))
    return total_warp_error

def map_warp_loss(x):
    y = x[0]
    y_ = x[1]
    out = calc_warp_loss(y, y_)
    print('----------------')
    return out

def calc_warp_loss(y, y_):
    pos = tf.boolean_mask(y, tf.not_equal(y_, 0.0), axis=0)
    neg = tf.boolean_mask(y, tf.equal(y_, 0.0), axis=0)
    # print("Positive values:\n {}".format(pos))
    # print("Negative values:\n {}".format(neg))

    weights = get_rank_weights(pos, neg)
    print('weights: {}'.format(weights))

    pos_ext = tf.expand_dims(pos, 1)
    neg_ext = tf.expand_dims(neg, 0)
    print("pos: {}".format(pos_ext))
    print("neg: {}".format(neg_ext))

    delta = tf.subtract(neg_ext, pos_ext)
    print('delta:\n{}'.format(delta))

    delta_adj = tf.multiply(delta, 5)
    print('delta_adj:\n{}'.format(delta_adj))

    exp = tf.exp(delta_adj)
    print('exp:\n{}'.format(exp))

    exp_weighted = exp * tf.expand_dims(weights, 1)
    print('exp_weighted:\n{}'.format(exp_weighted))

    sum_ = tf.reduce_sum(exp_weighted)
    print('sum: {}'.format(sum_))

    return sum_

# Calculates the weight for each positive label
def get_rank_weights(pos, neg):
    weights = tf.zeros_like(pos)

    neg = tf.expand_dims(neg, 0)
    pos = tf.expand_dims(pos, 1)

    # print('negative probs: {}'.format(neg))
    # print('positive probs:\n{}'.format(pos))
    less = tf.less(pos, neg)
    weights = tf.map_fn(calc_rank_weight, less, dtype=tf.float32)

    return weights

def calc_rank_weight(x):
    # random shuffle here, so each positive label is compared to a different order of negative labels
    x = tf.random_shuffle(x)
    num_labels = (tf.shape(x))
    # Add True in the end if there is no single True in the list
    x = tf.concat([x[:-1], [True]], axis=0)
    # num_labels = tf.cast((tf.shape(x)), tf.int64)
    num_samples = tf.reduce_min(tf.where(x)) + 1 # The plus 1 in the denominator accounts for starting to count at 0
    # print('number of samples: {}'.format(num_samples))
    rank = tf.cast(tf.floor((NUM_CLASSES-1)/(num_samples)), tf.int32)
    # print('rank: {}'.format(rank))
    alphas = 1/tf.range(1, NUM_CLASSES)
    weight = tf.reduce_sum(alphas[:rank])

    return tf.cast(weight, tf.float32)


def calc_rank_weight_all(x):
    # random shuffle here, so each positive label is compared to a different order of negative labels
    num_labels = tf.cast((tf.shape(x)), tf.float32)
    num_false = tf.reduce_sum(tf.cast(x, tf.float32))
    print('number of samples: {}'.format(num_labels))
    rank = tf.cast(tf.floor(num_labels/num_false), tf.int32)
    print('rank: {}'.format(rank))
    alphas = 1/tf.range(1, num_labels)
    weight = tf.reduce_sum(alphas[:rank])

    return tf.cast(weight, tf.float32)


if __name__ == '__main__':
    main()
