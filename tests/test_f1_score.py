import tensorflow as tf

tf.enable_eager_execution()


beta = 1
P = tf.constant([[1.0, 1.0, 1.0, 0.0], [0.0, 0.0, 0.0, 0.0], [1.0, 0.0, 1.0, 1.0]])
Y = tf.constant([[0.0, 1.0, 1.0, 0.0], [0.0, 1.0, 1.0, 0.0], [1.0, 1.0, 1.0, 0.0]])

P = tf.round(P)

TP = P * Y
print(TP)
FN_2TP_FP = P + Y
print(FN_2TP_FP)
micro_F1 = 2 * tf.reduce_sum(TP)/tf.reduce_sum(FN_2TP_FP)
print(micro_F1)
macro_F1 = 2 * tf.reduce_mean(tf.reduce_sum(TP, 0)/tf.reduce_sum(FN_2TP_FP,0))
print(macro_F1)
