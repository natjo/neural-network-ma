import tensorflow as tf

tf.enable_eager_execution()


# Y = tf.constant([0.9, 0.4, 0.2, 0.9])
# X = tf.constant([1.0, 0.3, 0.0, 0.1])
Y = tf.constant([[0.9, 0.4, 0.2], [0.9, 0.4, 0.2], [0.9, 0.4, 0.2], [0.9, 0.4, 0.2]])
X = tf.constant([[1.0, 1.0, 1.0], [0.0, 1.0, 0.0], [1.0, 0.0, 0.0], [0.0, 1.0, 0.0]])


[N, D] = tf.shape(X)
print(N)

# Calculate distance between X and Y
loss_dist = tf.reduce_sum(tf.square(X-Y))
# print(dist)


# Calculate orthogonality of X and Y
# X' * X, X has shape <num_examples x num_labels>
outerX = tf.matmul(tf.transpose(X), X)
print(outerX)

squared = tf.square(outerX - tf.eye(D))
print(squared)

loss_orth_X = tf.reduce_sum(squared) / tf.cast(N, tf.float32)
print(loss_orth_X)

# And Y
outerY = tf.matmul(tf.transpose(Y), Y)
loss_orth_Y = tf.reduce_sum(tf.square(outerY - tf.eye(D))) / tf.cast(N, tf.float32)


# Final calculation
lambdaX = 1
lambdaY = 1

CCA_dist = loss_dist + lambdaX * loss_orth_X + lambdaY * loss_orth_Y
print(CCA_dist)
