Tensorflow implementation of the paper 'Learning Deep Latent Spaces for Multi-Label Classfications' published in AAAI 2017.

Main code can be found in ´utils/network.py´.