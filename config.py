# LOG_DIR = './log/random_search/'
LOG_DIR = './log/current/'
LOG_NAME = 'bce'
FILE_WITH_TIME = True
BATCH_SIZE = 500
SHUFFLE_BUFFER_SIZE = 250 # Number of elements the next random sample will be drawn from
STEEPNESS = 5.0  # 5 in matlab code
RANKING_MARGIN = 1.0 # exp(STEEPNESS * (RANKING_MARGIN + p+ - p-))
EPOCHS = 50  # 50 in matlab code
LOG_TEST_INTERVAL = 1
SIZE_TEST = 2083  # All training examples
DROPOUT = 1.00  # 1 in paper == no dropout
PERC_WITH_DROPOUT = 0.0  # Percentage of epochs until dropout will turn to 1 (no dropout)
L2PENALTY = 1e-5 # 1e-3 in the matlab code
LEARNING_RATE = 1e-4  # 1e-4 in matlab code
LR_DECAY = 0.98  # 0.98 in paper
GRAD_DECAY = 0.9  # 0.98 in paper, decay of older gradients during rmsprop
MOMENTUM = 0.99
BETA1 = 0.9
BETA2 = 0.9  # Defaults to 0.999
LEAKY_RELU_ALPHA = 0.1

LAMBDA = 0.5  # Just for convenience, since the two used LAMBDAS will quite likely be the same
LAMBDA_X = LAMBDA
LAMBDA_Y = LAMBDA

# DIMENSIONS_INPU_DATA = [28, 28, 1] # 28x28 grey scale image
SIZE_ENC_FEATURES1 = 512
SIZE_ENC_FEATURES2 = 512
SIZE_ENC_LABELS1 = 512
SIZE_ENC_OUTPUT = 30
SIZE_DEC1 = 51
