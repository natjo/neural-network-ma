import tensorflow as tf
import config
from scipy.io import loadmat
import math
from tqdm import tqdm


def load_data():
    with tf.name_scope('dataset'):
        m = loadmat('data/mirflickr')
        x_train = m['X1']
        y_train = m['X2']
        x_test = m['XV1']
        y_test = m['XV2']
        print('Loaded {}/{} training features/labels and {}/{} test features/labels'.format(*[len(x) for x in [x_train, y_train, x_test, y_test]]))

        num_batches = math.ceil(len(x_train)/config.BATCH_SIZE)
        size_output = len(y_train[0])

        # Create train dataset
        train_dataset = tf.data.Dataset.from_tensor_slices((x_train, y_train))
        train_dataset = train_dataset.shuffle(config.SHUFFLE_BUFFER_SIZE).batch(config.BATCH_SIZE)
        test_dataset = tf.data.Dataset.from_tensor_slices((x_test, y_test))
        test_dataset = test_dataset.shuffle(config.SHUFFLE_BUFFER_SIZE).batch(config.SIZE_TEST)

        # Create generic iterator
        iterator = tf.data.Iterator.from_structure(train_dataset.output_types, train_dataset.output_shapes)

        # Create initializer for the two datasets
        train_init_op = iterator.make_initializer(train_dataset)
        test_init_op = iterator.make_initializer(test_dataset)
        features, labels = iterator.get_next()

        return train_init_op, test_init_op, features, labels, size_output, num_batches


def sigmoid_zero_mean(ins, name):
    with tf.variable_scope(name):
        return tf.nn.sigmoid(ins) - 0.5


def reduce_mean_handle_nan(ins):
    return tf.reduce_mean(replace_nan_with_zero(ins), name='reduce_mean_handle_nan')

def reduce_sum_handle_nan(ins):
    return tf.reduce_sum(replace_nan_with_zero(ins), name='reduce_sum_handle_nan')


# transform nan to zeroes
def replace_nan_with_zero(ins):
    return tf.where(tf.is_nan(ins), tf.zeros_like(ins), ins)


def print_collection(collection):
    [print(x) for x in tf.get_collection(collection)]


def variable_summaries(var, name):
    """Attach a lot of summaries to a Tensor (for TensorBoard visualization)."""
    # var = replace_nan_with_zero(var)
    with tf.name_scope('summary_'+name):
        with tf.name_scope('summaries_' + name):
            mean = tf.reduce_mean(var)
        tf.summary.scalar('mean_' + name, mean)
        with tf.name_scope('stddev_' + name):
            stddev = tf.sqrt(tf.reduce_mean(tf.square(var - mean)))
        tf.summary.scalar('stddev_' + name, stddev)
        tf.summary.scalar('max_' + name, tf.reduce_max(var))
        tf.summary.scalar('min_' + name, tf.reduce_min(var))
        tf.summary.histogram(name, var)


def init_bar(epoch, num_batches):
    return tqdm(total=num_batches,
            desc="Epoch {}".format(epoch),
            ascii=True,
            ncols=120,
            bar_format='{desc}|{bar}| {n_fmt}/{total_fmt} [{elapsed}, {rate_fmt}{postfix}]',
            postfix={'-', 'loss', '-', 'micro_f1', '-', 'macro_f1'})


def update_bar_post(bar, l, micro_f1, macro_f1):
    bar.set_postfix(
            loss="{:0.3f}".format(l),
            micro_f1="{:0.3f}".format(micro_f1),
            macro_f1="{:0.3f}".format(macro_f1))

