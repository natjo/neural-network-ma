import tensorflow as tf
import time

class BaseNetwork():
    def __init__(self, networkName, logdir, fileWithTime):
        tf.set_random_seed(0)
        self._all_macro_f1 = []
        self._all_micro_f1 = []

        self._networkName = networkName
        self._fileWithTime = fileWithTime
        self._logdir = logdir

        self._merged = None
        self._train_writer = None
        self._test_writer = None

        self._sess = tf.Session()


    def init_session(self):
        self._merged = tf.summary.merge_all()
        init = tf.global_variables_initializer()

        train_file_name = self._logdir + '/train_' + self._networkName
        test_file_name = self._logdir + '/test_' + self._networkName
        if(self._fileWithTime):
            timeStr = time.strftime("%Y%m%d_%H%M%S")
            train_file_name += '.' + timeStr
            test_file_name += '.' + timeStr

        self._train_writer = tf.summary.FileWriter(train_file_name, self._sess.graph)
        self._test_writer = tf.summary.FileWriter(test_file_name)

        self._sess.run(init)


    def save_scores(self, micro_f1, macro_f1):
        self._all_micro_f1.append(micro_f1)
        self._all_macro_f1.append(macro_f1)


    def get_max_micro_f1(self):
        return max(self._all_micro_f1)


    def get_max_macro_f1(self):
        return max(self._all_macro_f1)
