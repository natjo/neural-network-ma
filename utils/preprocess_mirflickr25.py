import pandas as pd
import csv
import glob
import os
import re
import my_excepthook


TAG_DIR = './data/mirflickr25/tags/*.txt'
THUMBNAILS_DIR = './data/mirflickr25/thumbnails'
MULTI_HOT_DIR = './data/mirflickr25/multi_hot_encoding_tags'
ZERO_LABELS_FILES = './data/mirflickr25/zero_label_files'
ALL_LABELS_PATH = './data/mirflickr25/all_labels'
NUM_LABELS = 200


# Steps
# 1. Update the config variables above
# 2. Remove possibly existing old folder/files that would be recreated during this process:
#    unused_images, multi_hot_encoding_tags, all_labels, thumbnails
# 3. Use the convert2thumbnails.sh IMAGE_FOLDER script to resize the images, defaults to 64x64
# 4. Run this python script
# 5. Run move_zero_label_files.sh in order to get rid of images that have no corresponding tag file due to zero labels


# Preprocess tag files to be properly used
# Remove all labels that occur less than X times
# Turn label files into one hot encoded label files
def main():
    # Get all files names and the corresponding ip
    fnames = glob.glob(TAG_DIR)
    basenames = [os.path.basename(f) for f in fnames]
    labelIDs = [re.search(r'\d+', b).group() for b in basenames]

    all_labels = get_top_k_labels(NUM_LABELS)
    save_all_labels(all_labels)

    all_zero_label_ids = []

    for (f,i) in zip(fnames, labelIDs):
        # print('tag {}'.format(i))
        if is_non_empty_file(f):
            num_true_labels = tag2multi_hot(f, i, all_labels)
            if num_true_labels == 0:
                all_zero_label_ids.append(i)
        else:
            all_zero_label_ids.append(i)

    save_name_of_zero_label_ids(all_zero_label_ids)


def tag2multi_hot(labelPath, labelID, all_labels):
    instance_labels = get_label_strings(labelPath)
    instance_multi_hot = multi_hot_encoding(instance_labels, all_labels)
    sum_true_labels = sum(instance_multi_hot)
    if sum_true_labels > 0:
        save_array(os.path.join(MULTI_HOT_DIR, 'tags{}.txt'.format(labelID)), instance_multi_hot)
    return sum_true_labels


def get_top_k_labels(k):
    df = pd.read_csv('./data/mirflickr25/doc/common_tags.txt', delimiter=' ',names=['label', 'occurences'], header=None)
    nLargest = df.nlargest(k, 'occurences')
    return nLargest['label']


def get_label_strings(labelPath):
    df = pd.read_csv(labelPath, header=None)
    return [x[0] for x in df.values]


def multi_hot_encoding(instance_labels, all_labels):
    return [1 if x in instance_labels else 0 for x in all_labels]


def save_name_of_zero_label_ids(ids):
    abs_path = os.path.abspath(THUMBNAILS_DIR)
    fileNames = [os.path.join(abs_path, 'thumb_im{}.jpg'.format(i)) for i in ids]
    with open(ZERO_LABELS_FILES, 'w+') as f:
        writer = csv.writer(f, delimiter=' ')
        for row in fileNames:
            writer.writerow([row])


def save_all_labels(allLabels):
    with open(ALL_LABELS_PATH, 'w') as f:
        writer = csv.writer(f, delimiter=' ')
        for row in allLabels:
            writer.writerow([row])


def save_array(path, array):
    with open(path, 'w') as f:
        writer = csv.writer(f, delimiter=' ')
        for row in array:
            writer.writerow([row])


def is_non_empty_file(fpath):
    return True if os.path.isfile(fpath) and os.path.getsize(fpath) > 0 else False

if __name__ == '__main__':
    main()
