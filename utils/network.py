import os
os.environ['TF_CPP_MIN_LOG_LEVEL']='2' # Don't tell me I'm using more than 10% of my memory
import tensorflow as tf
from utils.baseNetwork import BaseNetwork
import math
from utils.helpers import variable_summaries, load_data, init_bar, update_bar_post, print_collection, sigmoid_zero_mean,  reduce_mean_handle_nan, reduce_sum_handle_nan
from utils.loss_functions import define_ranking_loss, define_cross_entropy, define_binary_cross_entropy, define_log_sum_exp_pairwise_loss, define_warp_loss
import config
import time


class Network(BaseNetwork):
    def __init__(self, networkName, fileWithTime=True):
        super(Network, self).__init__(networkName, config.LOG_DIR, fileWithTime)

        self._train_init_op, self._test_init_op, self._features, self._labels, self._size_output, self._num_batches = load_data()

        self.define_structure()
        self.define_losses()
        self.define_optimizer()
        self.define_metrics()

        self.init_session()




    def define_structure(self):
        # The first dimension (None) will index the images in the mini-batch
        # self.X = tf.placeholder(tf.float32, [None].extend(DIMENSIONS_INPU_DATA), name='input')
        # self.Y_ = tf.placeholder(tf.float32, [None, SIZE_OUTPUT], name='ground_truth')
        self._step = tf.placeholder_with_default(1, shape=None,  name='current_step')
        self._pkeep = tf.placeholder_with_default(1.0, shape=None, name='dropout_probability')
        self._predictFromFeatures = tf.placeholder(tf.bool, shape=(), name='decode_features')

        # XX = tf.reshape(self._features, [-1, 784])

        # Define the label encoding layers
        E_L1 = self.define_fc_layer(tf.nn.leaky_relu, self._labels, config.SIZE_ENC_LABELS1, 'enc_labels1', hasDropout=True)
        self._labels_encoded = self.define_fc_layer(sigmoid_zero_mean, E_L1, config.SIZE_ENC_OUTPUT, 'enc_labels2', hasDropout=False)

        # Define the feature encoding layers
        E_F1 = self.define_fc_layer(tf.nn.leaky_relu, self._features, config.SIZE_ENC_FEATURES1, 'enc_feat1', hasDropout=True)
        E_F2 = self.define_fc_layer(tf.nn.leaky_relu, E_F1, config.SIZE_ENC_FEATURES2, 'enc_feat2', hasDropout=True)
        self._features_encoded = self.define_fc_layer(sigmoid_zero_mean, E_F2, config.SIZE_ENC_OUTPUT, 'enc_feat3', hasDropout=False)

        # Switch between encoded features and encoded labels
        encoded_input = tf.cond(self._predictFromFeatures,
                true_fn=lambda: self._features_encoded,
                false_fn=lambda: self._labels_encoded)

        # Define the label decoding layers
        D1 = self.define_fc_layer(tf.nn.leaky_relu, encoded_input, config.SIZE_DEC1, 'dec_labels1', hasDropout=True)
        self._prediction = self.define_fc_layer(tf.nn.sigmoid, D1, self._size_output, 'dec_labels2', hasDropout=False, nameVarBeforeActFun='_outputBeforeSigmoid')


    # if setVarBeforeActFun is True the output before the activation function
    def define_fc_layer(self, actFun, inputTensor, outputDim, layer_name, hasDropout=True, collections=[], nameVarBeforeActFun=None):
        inputDim = int(inputTensor.get_shape()[1])  # Automatically determine input dimension
        # Initialization as in the matlab script
        with tf.variable_scope(layer_name):
            if actFun == tf.nn.leaky_relu:
                init_w = tf.random_uniform([inputDim, outputDim], minval=-1, maxval=1) * 0.01
                init_b = tf.random_uniform([outputDim], minval=0, maxval=1) * 0.1
                W = tf.get_variable('W', initializer=init_w, regularizer=tf.nn.l2_loss)
                b = tf.get_variable('b', initializer=init_b)  # Don't regularize bias
                m = tf.matmul(inputTensor, W) + b
                Y = actFun(m, alpha=config.LEAKY_RELU_ALPHA, name='layer_output')
            elif actFun == tf.nn.sigmoid or actFun == sigmoid_zero_mean:
                init_w = tf.random_uniform([inputDim, outputDim], minval=-4, maxval=4) * tf.sqrt(6/(inputDim+outputDim))
                init_b = tf.zeros([outputDim])
                W = tf.get_variable('W', initializer=init_w, regularizer=tf.nn.l2_loss)
                b = tf.get_variable('b', initializer=init_b) # Don't regularize bias
                m = tf.matmul(inputTensor, W) + b
                Y = actFun(m, name='layer_output')
            else:
                raise ValueError('activation function of fc_layer unknown')

            # Adds a new member variable to this object with the name given by nameVarBeforeActFun
            if nameVarBeforeActFun:
                setattr(self, nameVarBeforeActFun, m)

            [tf.add_to_collection(c, W) for c in collections]
            [tf.add_to_collection(c, b) for c in collections]

            variable_summaries(W, 'weights')
            variable_summaries(b, 'biases')
            if hasDropout:
                return tf.nn.dropout(Y, self._pkeep, name='dropout')
            return Y


    def define_optimizer(self):
        with tf.variable_scope('optimizer'):
            lr = tf.train.exponential_decay(config.LEARNING_RATE, self._step, 1, config.LR_DECAY, name='learning_rate')
            # opt = tf.train.AdamOptimizer(learning_rate=lr, beta1=config.BETA1, beta2=config.BETA2)
            opt = tf.train.RMSPropOptimizer(learning_rate=lr, decay=config.GRAD_DECAY, momentum=config.MOMENTUM)
            self._optimizer = opt.minimize(self._overall_loss)


    def define_losses(self):
        with tf.variable_scope('losses'):
            with tf.variable_scope('binary_cross_entropy'):
                self._binary_cross_entropy = define_binary_cross_entropy(self._outputBeforeSigmoid, self._labels)

            with tf.variable_scope('ranking_loss'):
                self._ranking_loss = define_ranking_loss(self._prediction, self._labels)

            with tf.variable_scope('lsep_loss'):
                self._lsep_loss = define_log_sum_exp_pairwise_loss(self._prediction, self._labels)

            with tf.variable_scope('warp_loss'):
                self._warp_loss = define_warp_loss(self._prediction, self._labels, self._size_output)

            with tf.variable_scope('l2_loss'):
                var = tf.get_collection(tf.GraphKeys.REGULARIZATION_LOSSES)
                self._l2_loss = tf.reduce_sum(var) * config.L2PENALTY

            with tf.variable_scope('cca_loss'):
                # Calculate distance between X and Y
                loss_dist = tf.reduce_sum(tf.square(self._labels_encoded - self._features_encoded))

                # Calculate orthogonality of X and Y
                with tf.variable_scope('loss_enc_features'):
                    outerX = tf.matmul(tf.transpose(self._features_encoded), self._features_encoded)
                    loss_orth_X = config.LAMBDA_X * tf.reduce_sum(tf.square(outerX - tf.eye(config.SIZE_ENC_OUTPUT)))

                with tf.variable_scope('loss_enc_labels'):
                    outerY = tf.matmul(tf.transpose(self._labels_encoded), self._labels_encoded)
                    loss_orth_Y = config.LAMBDA_X * tf.reduce_sum(tf.square(outerY - tf.eye(config.SIZE_ENC_OUTPUT)))

                self._cca_loss = loss_dist + loss_orth_X + loss_orth_Y

                tf.summary.scalar('latent_distance', loss_dist)
                tf.summary.scalar('latent_loss_features', loss_orth_X)
                tf.summary.scalar('latent_loss_labels', loss_orth_Y)


            # self._overall_loss = self._ranking_loss + self._cca_loss + self._l2_loss
            self._overall_loss = self._binary_cross_entropy + self._cca_loss + self._l2_loss
            # self._overall_loss = self._lsep_loss + self._cca_loss + self._l2_loss
            # self._overall_loss = self._warp_loss + self._cca_loss + self._l2_loss

            tf.summary.scalar('binary_cross_entropy', self._binary_cross_entropy)
            tf.summary.scalar('ranking_loss', self._ranking_loss)
            tf.summary.scalar('lsep_loss', self._lsep_loss)
            tf.summary.scalar('warp_loss', self._warp_loss)
            tf.summary.scalar("l2_loss", self._l2_loss)
            tf.summary.scalar("cca_loss", self._cca_loss)
            tf.summary.scalar("overall_loss", self._overall_loss)


    def define_metrics(self):
        with tf.variable_scope('metrics'):
            round_prediction = tf.round(self._prediction)

            # Variables are named by the subsets of binary classifications they represent
            TP = round_prediction * self._labels
            FN_2TP_FP = round_prediction + self._labels
            TP_FP = round_prediction
            TP_FN = self._labels

            with tf.variable_scope('f1_score'):
                self._micro_f1 = 2 * tf.reduce_sum(TP)/tf.reduce_sum(FN_2TP_FP)
                self._macro_f1 = 2 * reduce_mean_handle_nan(tf.reduce_sum(TP, 0)/tf.reduce_sum(FN_2TP_FP,0))
            with tf.variable_scope('precision'):
                self._micro_precision = tf.reduce_sum(TP)/reduce_sum_handle_nan(TP_FP)
                self._macro_precision = reduce_mean_handle_nan(tf.reduce_sum(TP, 0)/tf.reduce_sum(TP_FP,0))
            with tf.variable_scope('recall'):
                self._micro_recall = tf.reduce_sum(TP)/tf.reduce_sum(TP_FN)
                self._macro_recall = reduce_mean_handle_nan(tf.reduce_sum(TP, 0)/tf.reduce_sum(TP_FN,0))

            tf.summary.scalar('micro_F1', self._micro_f1)
            tf.summary.scalar('macro_F1', self._macro_f1)
            tf.summary.scalar('micro_precision', self._micro_precision)
            tf.summary.scalar('macro_precision', self._macro_precision)
            tf.summary.scalar('micro_recall', self._micro_recall)
            tf.summary.scalar('macro_recall', self._macro_recall)


    def train(self):
        for epoch in range(config.EPOCHS):

            if epoch % config.LOG_TEST_INTERVAL == 0:
                # Save metrics for after the epoch to print it
                test_micro_f1, test_macro_f1, test_l = self.test_network(epoch)

            self._sess.run(self._train_init_op)  # Switch to training data
            progBar = init_bar(epoch, self._num_batches)

            # The summary needs to be updated only in the first batch
            updateSummary = True
            while True:
                try:
                    self.train_step(epoch, progBar, updateSummary)
                    updateSummary = False
                except tf.errors.OutOfRangeError:
                    break

            # Print information about last test run
            update_bar_post(progBar, test_l, test_micro_f1, test_macro_f1)
            progBar.close()


    def train_step(self, epoch, bar, updateSummary):
        if epoch < config.PERC_WITH_DROPOUT * config.EPOCHS:
            pkeep = config.DROPOUT
        else:
            pkeep = 1
        m, micro_f1, macro_f1, l_c, l_r, _ = self._sess.run(
            [self._merged, self._micro_f1, self._macro_f1, self._cca_loss, self._ranking_loss, self._optimizer],
            feed_dict={self._step:epoch, self._predictFromFeatures:False, self._pkeep:pkeep})
        update_bar_post(bar, l_c+l_r, micro_f1, macro_f1)
        bar.update(1)

        if(updateSummary):
            self._train_writer.add_summary(m, epoch)


    def test_network(self, epoch):
        self._sess.run(self._test_init_op)  # Switch to test data
        m, micro_f1, macro_f1, l_c, l_r = self._sess.run(
                [self._merged, self._micro_f1, self._macro_f1, self._cca_loss, self._ranking_loss],
                feed_dict={self._predictFromFeatures:True})
        self._test_writer.add_summary(m, epoch)
        self.save_scores(micro_f1, macro_f1)
        return micro_f1, macro_f1, l_c + l_r
