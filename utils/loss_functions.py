import tensorflow as tf
import config
from utils.helpers import reduce_mean_handle_nan


# See paper "Deep Learning for Extreme Multi-label Text Classification"
def define_binary_cross_entropy(Y, Y_):
    # return -tf.reduce_mean(tf.reduce_sum(Y_ * tf.log(Y) + (1-Y_) * tf.log(1-Y), axis=1))
    sce = tf.nn.sigmoid_cross_entropy_with_logits(
            logits=Y,
            labels=Y_)
    # Only take the mean over the number of elements, not over the number of labels
    return tf.reduce_mean(tf.reduce_sum(sce, axis=1))


def define_cross_entropy(Y, Y_):
    return -tf.reduce_mean(Y_ * tf.log(Y), name='cross_entropy')


def define_warp_loss(prediction, labels):
    stacked = tf.stack([prediction, labels], axis =1)
    elementwise_error = tf.map_fn(map_ranking_loss, stacked)
    total_ranking_error = reduce_mean_handle_nan(elementwise_error)
    return total_ranking_error


def define_ranking_loss(prediction, labels):
    stacked = tf.stack([prediction, labels], axis =1)
    elementwise_error = tf.map_fn(map_ranking_loss, stacked)
    total_ranking_error = reduce_mean_handle_nan(elementwise_error)
    return total_ranking_error


def map_ranking_loss(x):
    out = calc_ranking_loss(x[0], x[1])
    return out


def calc_ranking_loss(y, y_):
    pos = tf.boolean_mask(y, tf.not_equal(y_, 0.0), axis=0, name='positive_labels')
    neg = tf.boolean_mask(y, tf.equal(y_, 0.0), axis=0, name='negative_labels')

    pos_ext = tf.expand_dims(pos, 1)
    neg_ext = tf.expand_dims(neg, 0)

    delta = config.RANKING_MARGIN + neg_ext - pos_ext
    delta_adj = tf.multiply(delta, config.STEEPNESS, name='exponent')
    exp = tf.exp(delta_adj)

    # sum = tf.reduce_sum(exp)
    # mean = tf.reduce_mean(exp)
    mean_no_nan = reduce_mean_handle_nan(exp)
    return mean_no_nan


# See paper "Improving Pairwise Ranking for Multi-label Image Classification" (LSEP loss)
def define_log_sum_exp_pairwise_loss(prediction, labels):
    stacked = tf.stack([prediction, labels], axis =1)
    elementwise_error = tf.map_fn(map_lsep_loss, stacked)
    total_ranking_error = reduce_mean_handle_nan(elementwise_error)
    return total_ranking_error


def map_lsep_loss(x):
    out = calc_lsep_loss(x[0], x[1])
    return out


def calc_lsep_loss(y, y_):
    pos = tf.boolean_mask(y, tf.not_equal(y_, 0.0), axis=0, name='positive_labels')
    neg = tf.boolean_mask(y, tf.equal(y_, 0.0), axis=0, name='negative_labels')

    pos_ext = tf.expand_dims(pos, 1)
    neg_ext = tf.expand_dims(neg, 0)

    delta = neg_ext - pos_ext
    delta_adj = tf.multiply(delta, config.STEEPNESS, name='exponent')
    exp = tf.exp(delta_adj)

    # sum = tf.reduce_sum(exp)
    # mean = tf.reduce_mean(exp)
    out = tf.log(config.RANKING_MARGIN + tf.reduce_sum(exp))

    return out


def define_warp_loss(prediction, labels, num_total_labels):
    stacked = tf.stack([prediction, labels], axis =1)
    elementwise_error = tf.map_fn(lambda x: calc_warp_loss(x[0], x[1], num_total_labels), stacked)
    total_warp_error = tf.reduce_mean(elementwise_error)
    return total_warp_error

def map_warp_loss(x):
    return calc_warp_loss(x[0], x[1])

def calc_warp_loss(y, y_, num_total_labels):
    pos = tf.boolean_mask(y, tf.not_equal(y_, 0.0), axis=0)
    neg = tf.boolean_mask(y, tf.equal(y_, 0.0), axis=0)

    weights = get_rank_weights(pos, neg, num_total_labels)

    pos_ext = tf.expand_dims(pos, 1)
    neg_ext = tf.expand_dims(neg, 0)

    delta = tf.subtract(neg_ext, pos_ext)
    delta_adj = tf.multiply(delta, 5)

    exp = tf.exp(delta_adj)
    exp_weighted = exp * tf.expand_dims(weights, 1)

    return tf.reduce_sum(exp_weighted)

# Calculates the weight for each positive label
def get_rank_weights(pos, neg, num_total_labels):
    weights = tf.zeros_like(pos)

    neg = tf.expand_dims(neg, 0)
    pos = tf.expand_dims(pos, 1)

    less = tf.less(pos, neg)
    weights = tf.map_fn(lambda x: calc_rank_weight(x, num_total_labels), less, dtype=tf.float32)

    return weights

def calc_rank_weight(x, num_total_labels):
    # random shuffle here, so each positive label is compared to a different order of negative labels
    x = tf.random_shuffle(x)
    # Add True in the end if there is no single True in the list
    x = tf.concat([x[:-1], [True]], axis=0)
    num_samples = tf.reduce_min(tf.where(x)) + 1 # The plus 1 in the denominator accounts for starting to count at 0
    rank = tf.cast(tf.floor((num_total_labels-1)/(num_samples)), tf.int32)
    alphas = 1/tf.range(1, num_total_labels)
    weight = tf.reduce_sum(alphas[:rank])

    return tf.cast(weight, tf.float32)
