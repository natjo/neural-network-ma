from pandas import read_csv
import seaborn as sns
import matplotlib.pyplot as plt
import my_excepthook

WINDOW_SIZE=10
NUM_ROWS=3
NUM_COLS=4

def main():
    df = read_csv('./log/random_search/log.csv', delimiter=',', header=0)
    # print(df.head())
    cnt = 0
    fix, axes = plt.subplots(nrows=NUM_ROWS, ncols=NUM_COLS)
    keys = df.keys()

    # The last two are the scores, first is ID
    for key in df.keys()[1:-2]:
        print('plot pane: ', int(cnt/(NUM_ROWS+1)), cnt%NUM_COLS)
        plot_single_pane(df, key, axes[int(cnt/(NUM_ROWS+1)),cnt%NUM_COLS])
        cnt +=1

    plt.show()


def plot_single_pane(df, id_, ax):
    df = df.sort_values(by=[id_])
    # df.plot(x=id_, y=['MAX_MACRO_F1','MAX_MICRO_F1'], ax=ax)
    sns.lineplot(x=df[id_], y=df['MAX_MACRO_F1'].rolling(WINDOW_SIZE).mean(), ax=ax)
    sns.lineplot(x=df[id_], y=df['MAX_MICRO_F1'].rolling(WINDOW_SIZE).mean(), ax=ax)


if __name__ == '__main__':
    main()
