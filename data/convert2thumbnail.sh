#!/bin/bash

# converts all images specified in the folder of the first command line argument to the size set by NEW_SIZE.
# It keeps aspect ratios and padds the surrounding by white to always ensure NEW_SIZE dimensions.
# The new images will be put in a thumbnail directory and are called thumb_<old name>.

NEW_SIZE=64x64

mkdir $1/thumbnails
for image in $1/*.jpg; do
    output=$1/thumbnails/thumb_${image##*/}
    convert $image -resize $NEW_SIZE -gravity center -background "rgb(255,255,255)" -extent $NEW_SIZE $output
done
