#!/bin/bash
dos2unix ./zero_label_files
mkdir unused_images
while IFS='' read -r line; do
    mv "$line" unused_images/
done < ./zero_label_files
